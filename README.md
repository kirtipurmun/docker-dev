# Visit Kirtipur development environment
## A municipality tourism website
This repository hosts the Kirtipur Municipality's _Visit Kirtipur_ docker configuration. Visit Kirtipur is a customised Wordpress site that has information about the municipality's attractions for tourists.

## This repository (development environment)
This repository hosts the Docker configuration for development and testing, importing a `git submodule` for managing the Wordpress customisation.
Here is a brief introduction to the structure of this repository:
```
+-- wp-content/        # The wp-content directory of our
                       # Wordpress installation. This is a git
                       # submodule that exists as a separate
                       # repo.
+-- docker-compose.yml # The Docker Compose configuration for development.
```

## Getting started with Visit Kirtipur development
You may use this repository as a starting point to develop the Visit Kirtipur Wordpress site. 